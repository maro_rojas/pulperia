﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Contratos
{
    [ExceptionShielding]
    [ServiceContract]
    public interface IHistorialPrecio
    {
        [OperationContract]
        bool ServicioDisponible();


        [OperationContract]
        [WebGet(UriTemplate = "HistorialPrecio", ResponseFormat = WebMessageFormat.Json)]
        List<Modelos.Contenedores.HistorialPrecio> ListarDesdeBaseDatos();
    }
}
