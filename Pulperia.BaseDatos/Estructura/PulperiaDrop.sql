USE Pulperia
go
IF OBJECT_ID('Articulo') IS NOT NULL
BEGIN
    DROP TABLE Articulo
    PRINT '<<< DROPPED TABLE Articulo >>>'
END
go
IF OBJECT_ID('ArticuloPromocion') IS NOT NULL
BEGIN
    DROP TABLE ArticuloPromocion
    PRINT '<<< DROPPED TABLE ArticuloPromocion >>>'
END
go
IF OBJECT_ID('HistorialPrecio') IS NOT NULL
BEGIN
    DROP TABLE HistorialPrecio
    PRINT '<<< DROPPED TABLE HistorialPrecio >>>'
END
go
IF OBJECT_ID('Promociones') IS NOT NULL
BEGIN
    DROP TABLE Promociones
    PRINT '<<< DROPPED TABLE Promociones >>>'
END
go


