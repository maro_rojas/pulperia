USE Pulperia
go/* 
 * TABLE: Articulo 
 */CREATE TABLE Articulo(
    idArticulo     int            IDENTITY(1,1),
    codArticulo    nchar(6)       NOT NULL,
    nombre         varchar(50)    NOT NULL,
    precio         int            NOT NULL,
    existencia     int            NOT NULL,
    codEstado      int            NOT NULL,
    CONSTRAINT PK_Articulos PRIMARY KEY CLUSTERED (idArticulo)
)
go



IF OBJECT_ID('Articulo') IS NOT NULL
    PRINT '<<< CREATED TABLE Articulo >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Articulo >>>'
go

/* 
 * TABLE: ArticuloPromocion 
 */

CREATE TABLE ArticuloPromocion(
    idArticulo       int    NOT NULL,
    idPromociones    int    NOT NULL,
    CONSTRAINT PK_intermedia PRIMARY KEY CLUSTERED (idArticulo, idPromociones)
)
go



IF OBJECT_ID('ArticuloPromocion') IS NOT NULL
    PRINT '<<< CREATED TABLE ArticuloPromocion >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE ArticuloPromocion >>>'
go

/* 
 * TABLE: HistorialPrecio 
 */

CREATE TABLE HistorialPrecio(
    idHistorialPrecio    int         IDENTITY(1,1),
    codArticulo          nchar(6)    NOT NULL,
    precio               int         NOT NULL,
    mes                  int         NOT NULL,
    CONSTRAINT PK_HistorialPrecio PRIMARY KEY CLUSTERED (idHistorialPrecio)
)
go



IF OBJECT_ID('HistorialPrecio') IS NOT NULL
    PRINT '<<< CREATED TABLE HistorialPrecio >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE HistorialPrecio >>>'
go

/* 
 * TABLE: Promociones 
 */

CREATE TABLE Promociones(
    idPromociones    int            NOT NULL,
    nombre           varchar(45)    NULL,
    valor            int            NULL,
    CONSTRAINT PK_Promociones PRIMARY KEY CLUSTERED (idPromociones)
)
go



IF OBJECT_ID('Promociones') IS NOT NULL
    PRINT '<<< CREATED TABLE Promociones >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Promociones >>>'
go

/* 
 * TABLE: ArticuloPromocion 
 */

ALTER TABLE ArticuloPromocion ADD CONSTRAINT FK__ArticuloP__idArt__276EDEB3 
    FOREIGN KEY (idArticulo)
    REFERENCES Articulo(idArticulo)
go

ALTER TABLE ArticuloPromocion ADD CONSTRAINT FK__ArticuloP__idPro__286302EC 
    FOREIGN KEY (idPromociones)
    REFERENCES Promociones(idPromociones)
go