﻿using Pulperia.Contratos;
using Pulperia.LogicaNegocio.Dominio.Caracteristicas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Pulperia.Modelos;

namespace Pulperia.LogicaNegocio.Servicios
{

	[ServiceBehavior]
	public class Articulo : IArticulo
	{
		[OperationBehavior]
		public bool ServicioDisponible()
		{
			return true;
		}
		[OperationBehavior]
		public List<Pulperia.Modelos.Articulo> ListarArticulos()
		{
			List<Pulperia.Modelos.Articulo> listaArticulos = new List<Pulperia.Modelos.Articulo>();

			listaArticulos.Add(new Pulperia.Modelos.Articulo { IdArticulo = 1, CodArticulo = "1", Nombre = "Azucar", Precio = 1600, Existencia = 10, CodEstado = 1 });
			listaArticulos.Add(new Pulperia.Modelos.Articulo { IdArticulo = 2, CodArticulo = "2", Nombre = "Arroz", Precio = 1200, Existencia = 10, CodEstado = 1 });
			listaArticulos.Add(new Pulperia.Modelos.Articulo { IdArticulo = 3, CodArticulo = "3", Nombre = "Frijoles", Precio = 1000, Existencia = 10, CodEstado = 1 });
			listaArticulos.Add(new Pulperia.Modelos.Articulo { IdArticulo = 4, CodArticulo = "4", Nombre = "Jabon", Precio = 1800, Existencia = 10, CodEstado = 1 });

			return listaArticulos;
		}

		[OperationBehavior]
		public List<Pulperia.Modelos.Articulo> ListarDesdeBaseDatos()
		{
			using (var repositorio = new AccesoDatos.Repositorios.Articulos())
			{
				return repositorio.Articulo.ToList();
			}
		}

		[OperationBehavior]
		public Pulperia.Modelos.Articulo ObtenerPorCodArticulo(string CodArticulo)
		{
			using(var rep = new AccesoDatos.Repositorios.Articulos())
			{
				var art = rep.Articulo.FirstOrDefault(x => x.CodArticulo == CodArticulo);
				return art;
			}
		}

		[OperationBehavior]
		public void InsertarArticulo(Pulperia.Modelos.Articulo nuevoArticulo)
		{
			var caracteristica = new AgregarArticulo();
			caracteristica.Agregar(nuevoArticulo);
		}

		[OperationBehavior]
		public void InactivarArticulo(Pulperia.Modelos.Articulo nuevoArticulo)
		{
			var caracteristica = new AgregarArticulo();
			caracteristica.Inactivar(nuevoArticulo);
		}

        [OperationBehavior]
		public void ActualizarArticulo(Pulperia.Modelos.Articulo nuevoArticulo)
        {
            var caracteristica = new AgregarArticulo();
            caracteristica.Actualizar(nuevoArticulo);
        }

        //public Modelos.Articulo InsertarArticulo(string codArticulo, string nombreArticulo, int precioArticulo, int existenciaArticulo, int codigoEstado)
        //{
        //	using(var rep = new AccesoADatos.Repositorios.Articulos())
        //	{
        //		var nuevoActiculo = new Modelos.Articulo();
        //		nuevoActiculo.CodArticulo = codArticulo;
        //		nuevoActiculo.Nombre = nombreArticulo;
        //		nuevoActiculo.Precio = precioArticulo;
        //		nuevoActiculo.Existencia = existenciaArticulo;
        //		nuevoActiculo.CodEstado = codigoEstado;
        //		rep.Articulo.Add(nuevoActiculo);
        //		rep.SaveChanges();
        //		int idart = nuevoActiculo.IdArticulo;
        //		return rep.Articulo.FirstOrDefault(x => x.IdArticulo == idart);
        //	}
        //}

		[OperationBehavior]
		public void LanzarExcepcion()
		{
			throw new Pulperia.Modelos.Excepciones.PulperiaException("Error codigo",1);
		}
	}
}
