﻿using Comunes;
using Comunes.Auditoria;
using Comunes.Auditoria.Extensiones.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Runtime;

namespace Pulperia.LogicaNegocio
{
	public class InitializeService
	{
		public static void appInitialize()
		{
			InjectionConstructor firma = new InjectionConstructor(new object[] { });

			//TODO: Registrar la instancia de los servicios cuando se tienen instacia a otros servicios de ese servicio
			//ContenedorUnity.Instancia.RegisterType<SinpeMovil.LogicaNegocio.Servicios.Promerica.Servicios.ServicioPromericaClient, SinpeMovil.LogicaNegocio.Servicios.Promerica.Servicios.ServicioPromericaClient>(firma);

			ContenedorUnity.Instancia.RegisterTypes(UnityHelpers.GetTypesWithCustomAttribute<UnityAutoRegistrationAttribute>(

		   AllClasses.FromLoadedAssemblies()),
		   i => WithMappings.FromMatchingInterface(i),
		   i => WithName.Default(i),
		   i => WithLifetime.Transient(i));

			var mappingsBaseException = new NameValueCollection()
			{
				{"IdManejo", "{Guid}"}
			};

			var mappingsPulperiaException = new NameValueCollection()
			{
				{"IdManejo", "{Guid}"},
				{"Mensaje", "{Message}"},
				{"Codigo", "{NumeroError}"}
			};

			var bitacoraHandler = new BitacoraExceptionHandler("Negocio",
																100,
																TraceEventType.Error,
																"Enterprise Library Exception Handling",
																(int)Modelos.Enumerados.PrioridadEvento.Media,
																typeof(Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter),
																null,
																true);

			var exceptionShieldingPolicy = new System.Collections.Generic.List<ExceptionPolicyEntry>()
			{
				new ExceptionPolicyEntry(typeof(System.Exception),
					PostHandlingAction.ThrowNewException,
					new IExceptionHandler[] {
						bitacoraHandler,
						new FaultContractExceptionHandler(typeof(Modelos.Excepciones.PulperiaFault),
							"Ocurrió un error en el procesamiento de la operación.",
							mappingsBaseException)
					}),
				new ExceptionPolicyEntry(typeof(Modelos.Excepciones.PulperiaException),
					PostHandlingAction.ThrowNewException,
					new IExceptionHandler[] {
						bitacoraHandler,
						new FaultContractExceptionHandler(typeof(Modelos.Excepciones.PulperiaFault),
							"Ocurrió un error en el procesamiento de la operación.",
							mappingsPulperiaException)
					})
			};

			var bitacoraPolicy = new List<ExceptionPolicyEntry>()
			{
				new ExceptionPolicyEntry(typeof(Exception),
					PostHandlingAction.NotifyRethrow,
					new IExceptionHandler[]
					{
						bitacoraHandler
					})
			};

			var policies = new List<ExceptionPolicyDefinition>()
			{
				new ExceptionPolicyDefinition("WCF Exception Shielding", exceptionShieldingPolicy),
				new ExceptionPolicyDefinition("Bitacora", bitacoraPolicy)
			};

			var exManager = new ExceptionManager(policies);
			ExceptionPolicy.Reset();
			ExceptionPolicy.SetExceptionManager(exManager);

		}
	}
}