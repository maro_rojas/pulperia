﻿using Comunes.Auditoria;
using Pulperia.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.LogicaNegocio.Dominio.Acciones
{
    public class Articulo
    {
		public void InactivarArticulo(Modelos.Articulo nuevoArticulo)
        {
            using (var rep = new Pulperia.AccesoDatos.Repositorios.Articulos())
            {
				Modelos.Articulo a = rep.Articulo.First(i => i.IdArticulo == nuevoArticulo.IdArticulo);
                a.CodEstado = (int)Modelos.Enumerados.Estados.Inactivo;
                rep.SaveChanges();
            }
        }

		public void GuardarArticulo(Modelos.Articulo nuevoArticulo)
        {
            using (var rep = new Pulperia.AccesoDatos.Repositorios.Articulos())
            {
                if (nuevoArticulo.CodEstado == (int)Modelos.Enumerados.Estados.Inactivo)
                {
                    nuevoArticulo.CodEstado = (int)Modelos.Enumerados.Estados.Activo;
                }
                rep.Articulo.Add(nuevoArticulo);
                rep.SaveChanges();
            }
			Bitacora.Escribir(string.Format("Se ha agregado el articulo con código {0}", nuevoArticulo.CodArticulo));
        }

		public Modelos.Articulo ObtenerArticuloPorCodigoArticulo(Modelos.Articulo articulo)
        {
            using (var rep = new Pulperia.AccesoDatos.Repositorios.Articulos())
            {
                return rep.Articulo.FirstOrDefault(x => x.CodArticulo == articulo.CodArticulo);
            }
        }

		public void ActualizarArticulo(Modelos.Articulo articulo)
        {
            using (var repositorio = new Pulperia.AccesoDatos.Repositorios.Articulos())
            {
				var a = repositorio.Articulo.First(i => i.CodArticulo == articulo.CodArticulo);
                a.Nombre = articulo.Nombre;
                a.Precio = articulo.Precio;
                a.Existencia = articulo.Existencia;
                a.CodEstado = articulo.CodEstado;
                repositorio.SaveChanges();
            }
        }
    }
}
