﻿using Comunes.Auditoria;
using Pulperia.LogicaNegocio.Dominio.Acciones;
using Pulperia.LogicaNegocio.Dominio.Validaciones;
using Pulperia.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Pulperia.LogicaNegocio.Dominio.Caracteristicas
{
	public class AgregarArticulo : CaracteristicaTransaccional<Modelos.Articulo>
	{

		public override bool Validar(Modelos.Articulo item)
		{
			var validador = new Dominio.Validaciones.ValidacionesArticulo();
			var resultado = validador.LaExistenciaNoEsMayorACero(item) &&
							validador.ElPrecioNoEsValido(item) &&
							validador.ElNombreNoDebeEstarVacio(item);

			if (!resultado)
				this.Excepciones.Add(validador.Excepcion);

			return resultado;
		}


		private bool ValidarSiElArticuloNoExiste(Modelos.Articulo articuloEncontrado)
		{
			var validador = new Dominio.Validaciones.ValidacionesArticulo();
			var resultado = validador.ElArticuloNoExiste(articuloEncontrado);

			if (!resultado)
				this.Excepciones.Add(validador.Excepcion);

			return resultado;
		}


		public void Agregar(Modelos.Articulo item)
		{
			var accion = new Pulperia.LogicaNegocio.Dominio.Acciones.Articulo();
			if (!Validar(item))
				throw this.Excepciones.FirstOrDefault();

			//TODO: Obtener el articulo de base de datos, verificar si existe y si existe verificar el estado y sino exite lo actualizar

			var articuloEncontrado = accion.ObtenerArticuloPorCodigoArticulo(item);

			//Existe en la BD
			if (!ValidarSiElArticuloNoExiste(articuloEncontrado))
				throw this.Excepciones.FirstOrDefault();
			else
			{
				using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
				{
					//TODO: Aquí el código para guardar utilizando una accion
					accion.GuardarArticulo(item);
					scope.Complete();

				}
				Bitacora.Escribir(string.Format("Se ha agregado el articulo con código {0}", item.CodArticulo));
			}
		}

		public void Inactivar(Modelos.Articulo item)
		{
			var accion = new Acciones.Articulo();

			//TODO: Obtener el articulo de base de datos, verificar si existe y si existe verificar el estado y sino exite lo actualizar
			var articuloEncontrado = accion.ObtenerArticuloPorCodigoArticulo(item);

			//Existe en la BD
			if (articuloEncontrado != null)
			{
				if (articuloEncontrado.CodEstado == (int)Modelos.Enumerados.Estados.Activo)
				{
					//ArticuloEncontradoPorCodArticulo.CodEstado = 2;
					accion.InactivarArticulo(articuloEncontrado);
				}
				else
					throw this.Excepciones.FirstOrDefault();
			}
			else
				throw this.Excepciones.FirstOrDefault();
		}

		public void Actualizar(Modelos.Articulo item)
		{
			var accion = new Acciones.Articulo();

			if (!Validar(item))
				throw this.Excepciones.FirstOrDefault();

			//TODO:Obtener el articulo de base de datos, verificar si existe y si existe verificar el estado y sino exite lo actualizar
			var articuloEncontrado = accion.ObtenerArticuloPorCodigoArticulo(item);

			//Existe en la BD
			if (articuloEncontrado != null)
				accion.ActualizarArticulo(item);
			else // No Existe en la BD
				throw this.Excepciones.FirstOrDefault();
		}

	}
}
