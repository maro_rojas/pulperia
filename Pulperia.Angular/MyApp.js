﻿angular
    .module('MyApp', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
    	$routeProvider
            .when("/", { templateUrl: "partials/home.html" })
            .when("/articulos", { templateUrl: "partials/articulos.html", controller: ListarArticulos })
            .when("/articulo/edit/:id", { templateUrl: "partials/edit.html", controller: ObtenerArticulo })
            .when("/articulo/add", { templateUrl: "partials/add.html", controller: AgregarArticulo })
    }])
    .controller("MyCrtl", ["$scope", MyCrtl])

function MyCrtl($scope) {

}

function ListarArticulos($scope) {

	$(document).ready(function () {
		$("#grid").kendoGrid({
			dataSource: {
				type: "JSON",
				transport: {
					read: "http://localhost/Servicios/Pulperia.Servicios/Articulo.svc/Articulos"
				},
				pageSize: 20
			},
			height: 550,
			groupable: true,
			sortable: true,
			pageable: {
				refresh: true,
				pageSizes: true,
				buttonCount: 5
			},
			columns: [{
				template: "<div class='customer-name'>#: Nombre #</div>",
				field: "Nombre",
				title: "Nombre Articulo",
				width: 240
			}, {
				field: "Precio",
				title: "Precio"
			}, {
				field: "Existencia",
				title: "Existencia"
			}]
		});
	});
}

function ObtenerArticulo($scope, $location, $routeParams) {
	$.ajax({
		url: "http://localhost/Servicios/Pulperia.Servicios/Articulo.svc/ObtenerArticulos/" + $routeParams.id,
		type: "POST",
		dataType: "JSON"
	}).done(function (json) {
		$scope.articulo = { "CodArticulo": json.CodArticulo, "Nombre": json.Nombre, "Precio": json.Precio, "Existencia": json.Existencia, "CodEstado": json.CodEstado };
	}).fail(function () {

	});

	$scope.actualizarArticulo = function () {
		var jsonArticulo = { "CodArticulo": $scope.articulo.CodArticulo, "Nombre": $scope.articulo.Nombre, "Precio": $scope.articulo.Precio, "CodEstado": $scope.articulo.CodEstado, "Existencia": $scope.articulo.Existencia };

		$.ajax({
			url: "http://localhost/Servicios/Pulperia.Servicios/Articulo.svc/ActualizarArticulo",
			type: "POST",
			dataType: "JSON",
			contentType: 'application/json',
			data: JSON.stringify(jsonArticulo)
		}).done(function (json) {
			$scope.CodArticulo = json.CodArticulo;
			$scope.Nombre = json.Nombre;
			$scope.Precio = json.Precio;
			$scope.Existencia = json.Existencia;
			$scope.CodEstado = json.CodEstado;

		}).fail(function () {

		});
		$location.path("/articulos");
	}
}

function AgregarArticulo($scope, $location) {
	$scope.agregarArticulo = function () {
		var jsonArticulo = { "CodArticulo": $scope.CodArticulo, "Nombre": $scope.Nombre, "Precio": $scope.Precio, "CodEstado": $scope.CodEstado, "Existencia": $scope.Existencia };
		$.ajax({
			url: "http://localhost/Servicios/Pulperia.Servicios/Articulo.svc/InsertarArticulo",
			type: "POST",
			dataType: "JSON",
			contentType: 'application/json',
			data: JSON.stringify(jsonArticulo)
		}).done(function (json) {

		}).fail(function () {

		});
		$location.path("/articulos");
	}
}
