﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Certificados
{
    public class CertificateInfo
    {
        public X509Certificate GetCertificate(string subject)
        {
            X509Store storex = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            X509Certificate certificatex=null;
            try
            {
                storex.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection certificatesx = storex.Certificates.Find(X509FindType.FindBySubjectDistinguishedName, subject,false);
                certificatex = certificatesx[0];
            }
            finally
            {
                storex.Close();
            }
            return certificatex;
        }

        public X509Certificate GetCertificatesList()
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            X509Certificate cer = null;
            try
            {
                store.Open(OpenFlags.ReadOnly);
                var certificates = store.Certificates;
                cer = store.Certificates[0];

                foreach (var certificate in certificates)
                {
                    var friendlyName = certificate.FriendlyName;
                    var xname = certificate.SubjectName; //obsolete
                    Console.WriteLine(friendlyName);
                }
            }
            finally
            {
                store.Close();
            }
            return cer;
        }
        
        public static X509Store CreateX509Store()
        {
            return new X509Store();
        }
    }
}
