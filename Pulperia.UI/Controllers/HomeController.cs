﻿using Pulperia.Modelos;
using Pulperia.UI.Servicio.Articulo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pulperia.UI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Index
        public ActionResult Index()
        {
			var servicio = new Servicio.Articulo.ArticuloClient();
			try
			{
				return View(servicio.ListarDesdeBaseDatos());
			}
			finally
			{
				servicio.Close();
			}
        }
		public ActionResult ObtenerPorID()
		{
			return View();
		}

	[HttpPost]
		public ActionResult BuscarPorCod(Articulo art)
		{
			var servicio = new Servicio.Articulo.ArticuloClient();
			try
			{
				Articulo arti = servicio.ObtenerPorCodArticulo(art.CodArticulo);
				return PartialView(arti);
			}
			finally
			{
				servicio.Close();
			}
		}        

		public ActionResult AgregarArticulo()
		{
			Articulo nuevoArticulo = new Articulo();
			return View(nuevoArticulo);
		}

		[HttpPost]
		public ActionResult AgregarArticulo(Articulo nuevoArticulo)
		{
			var servicio = new ArticuloClient();
			try
			{
				servicio.InsertarArticulo(nuevoArticulo);
			}
			finally
			{
				servicio.Close();
			}
			return RedirectToAction("Index");
		}

		[HttpGet]
		public ActionResult ActualizarArticulo(string id)
		{
			var servicio = new ArticuloClient();
			try
			{
				Articulo articulo = servicio.ObtenerPorCodArticulo(id);
				return View(articulo);
			}
			finally
			{
				servicio.Close();
			}
		}

		[HttpPost]
		public ActionResult ActualizarArticulo(Articulo nuevoArticulo)
		{
            var mensajeError = "";
			var servicio = new ArticuloClient();
			try
			{
                servicio.ActualizarArticulo(nuevoArticulo);
            }
            catch (System.ServiceModel.FaultException<Modelos.Excepciones.PulperiaFault> pulperiaFault)
            {
                mensajeError = pulperiaFault.Detail.Mensaje;
            }
            finally
			{
				servicio.Close();
			}
			return RedirectToAction("Index");
		}
    }
}