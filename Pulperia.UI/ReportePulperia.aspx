﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportePulperia.aspx.cs" Inherits="Pulperia.UI.ReportePulperia" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    	<rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="468px" ProcessingMode="Remote" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="803px">
			<ServerReport ReportPath="/ReportePulperia" />
		</rsweb:ReportViewer>
		<asp:ScriptManager ID="ScriptManager1" runat="server">
		</asp:ScriptManager>
    </form>
</body>
</html>
