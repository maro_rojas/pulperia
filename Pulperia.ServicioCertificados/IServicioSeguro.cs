﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Pulperia.ServicioCertificados
{
    [ServiceContract(Namespace = "http://www.formsevolution.com")]
    public interface IServicioSeguro
    {
        [OperationContract]
        int Sumar(int pX, int pY);
    }
}
