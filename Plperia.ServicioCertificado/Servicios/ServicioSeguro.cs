﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Plperia.ServicioCertificado
{
    [ServiceBehavior]
    public class ServicioSeguro : IServicioSeguro
    {
        [OperationBehavior]
        public bool ServicioDisponible()
        {
            return true;
        }
    }
}
