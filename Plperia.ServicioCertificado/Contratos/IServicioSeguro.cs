﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Plperia.ServicioCertificado
{
    [ServiceContract(Namespace = "http://CRUX.CONSULTORES.COM.Destino")]
    public interface IServicioSeguro
    {
        [OperationContract]
        bool ServicioDisponible();
    }
}
