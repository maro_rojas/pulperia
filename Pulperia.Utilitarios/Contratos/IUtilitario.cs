﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Utilitarios.Contratos
{
	[ServiceContract]
	public interface IUtilitario
	{
		[OperationContract]
		bool ServicioDisponible();

		[OperationContract]
		void EliminarArticulos(string codigoArticulo);

		[OperationContract]
		void InsertarExistentes(Pulperia.Modelos.Articulo articulo);

		[OperationContract]
		void InactivarInactivos(Pulperia.Modelos.Articulo articulo);

		[OperationContract]
		Pulperia.Modelos.Articulo ObtenerPorCodArticulo(string CodArticulo);
	}
}
