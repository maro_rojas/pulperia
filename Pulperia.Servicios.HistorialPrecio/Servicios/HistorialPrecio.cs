﻿using Pulperia.Contratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Pulperia.Servicios.HistorialPrecio
{
    [ServiceBehavior]
    public class HistorialPrecio : IHistorialPrecio
    {
        [OperationBehavior]
        public bool ServicioDisponible()
        {
            return true;
        }

        [OperationBehavior]
        public List<Modelos.Contenedores.HistorialPrecio> ListarDesdeBaseDatos()
        {
            using (var repositorio = new AccesoDatos.Repositorios.HistorialPrecios())
            {
                return repositorio.HistorialPrecio.ToList();
            }
        }
    }
}
