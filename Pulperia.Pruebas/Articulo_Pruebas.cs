﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pulperia.Pruebas.Servicio.Articulo;
using Pulperia.Modelos;
using System.Collections.Generic;
using System.ServiceModel;
using Pulperia.Modelos.Excepciones;

namespace Pulperia.Pruebas
{
    [TestClass]
    public class Articulo_Pruebas
    {
        [TestMethod]
        public void ServicioDisponible_Pruebas()
        {
            var resultado = false;
            var servicio = new ArticuloClient();
            try
            {
                resultado = servicio.ServicioDisponible();

            }
            finally
            {
                servicio.Close();
            }
            Assert.IsTrue(resultado);
        }

        [TestMethod]
        public void ListaArticulos_Pruebas()
        {
            var servicio = new ArticuloClient();
			var listaArticulosEsperados = new List<Pulperia.Modelos.Articulo>()
			{
				new Pulperia.Modelos.Articulo { IdArticulo = 1, CodArticulo = "1", Nombre = "Azucar", Precio = 1600, Existencia = 10, CodEstado = 1 },
				new Pulperia.Modelos.Articulo { IdArticulo = 2, CodArticulo = "2", Nombre = "Arroz", Precio = 1200, Existencia = 10, CodEstado = 1 },
				new Pulperia.Modelos.Articulo { IdArticulo = 3, CodArticulo = "3", Nombre = "Frijoles", Precio = 1000, Existencia = 10, CodEstado = 1 },
				new Pulperia.Modelos.Articulo { IdArticulo = 4, CodArticulo = "4", Nombre = "Jabon", Precio = 1800, Existencia = 10, CodEstado = 1 }
			};

            List<Pulperia.Modelos.Articulo> listaArticulosObtenidos = null;
            try
            {
				listaArticulosObtenidos = servicio.ListarArticulos();

            }
            finally
            {
                servicio.Close();
            }
			Assert.IsNotNull(listaArticulosObtenidos);
			Assert.IsTrue(listaArticulosObtenidos.Count > 0);
			CollectionAssert.AreEqual(listaArticulosEsperados, listaArticulosObtenidos);
        }

        [TestMethod]
        public void ListaArticulosBD_Pruebas()
        {
            var servicio = new ArticuloClient();
            List<Pulperia.Modelos.Articulo> listaArticulos = null;
            try
            {
                listaArticulos = servicio.ListarDesdeBaseDatos();
            }
            finally
            {
                servicio.Close();
            }
            Assert.IsNotNull(listaArticulos);
            Assert.IsTrue(listaArticulos.Count > 0);
        }

        [TestMethod]
        public void ObtenerPorId_Pruebas()
        {
            var servicio = new ArticuloClient();
            Pulperia.Modelos.Articulo articulo = null;
            try
            {
                articulo = servicio.ObtenerPorCodArticulo("26");
            }
            finally
            {
                servicio.Close();
            }
            Assert.IsNotNull(articulo);
            Assert.AreEqual("Arroz", articulo.Nombre);
        }

        [TestMethod]
        public void InsertarArticulo_Pruebas()
        {
            var servicio = new ArticuloClient();
            //Pulperia.Modelos.Articulo articulo = null;
            var articulo = new Modelos.Articulo()
                {
                    CodArticulo = "asd123",
                    Nombre = "prueba",
                    Existencia = 10,
                    CodEstado = 2,
                    Precio = 100
                };
            try
            {
                servicio.InsertarArticulo(articulo);
            }
            finally
            {
                servicio.Close();
            }
            Assert.IsNotNull(articulo);
            Assert.AreEqual("prueba", articulo.Nombre);
        }

        [TestMethod]
        public void ActualizarArticulo_Pruebas()
        {
            var servicio = new ArticuloClient();
            //Pulperia.Modelos.Articulo articulo = null;
            var articulo = new Modelos.Articulo()
            {
                CodArticulo = "bnm123",
                Nombre = "algo",
                Existencia = 10,
                CodEstado = 1,
                Precio = 100
            };
            Modelos.Articulo a = null;
            try
            {
                servicio.ActualizarArticulo(articulo);
                a = servicio.ObtenerPorCodArticulo(articulo.CodArticulo);
            }
            finally
            {
                servicio.Close();
            }
            Assert.IsNotNull(a);
            Assert.AreEqual("algo", a.Nombre);
        }

        [TestMethod]
        public void LanzarPrueba_Pruebas()
        {
            var errorEsperado = "Error codigo";
            var errorObtenido = "";
            var servicio = new ArticuloClient();
            try
            {
                servicio.LanzarExcepcion();
            }
            catch (FaultException<PulperiaFault> ex)
            {
                errorObtenido = ex.Detail.Mensaje;
            }
            finally
            {
                servicio.Close();
            }
            Assert.AreEqual(errorEsperado, errorObtenido);
        }

        [TestMethod]
        public void PruebaCertificado_Pruebas()
        {
            var certificado = new Pulperia.Certificados.CertificateInfo();
            //var cex = certificado.GetCertificatesList();
            var cex = certificado.GetCertificate("CN=localhost");
            Assert.IsNotNull(cex);
        }
    }
}
