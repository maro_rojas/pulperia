﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Pulperia.Pruebas
{

	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void LanzarExcepcion_Prueba()
		{
			var esperado = "Mensaje de error";
			var obtenido = "";
			var servicio = new Pulperia.Pruebas.Servicio.Articulo.ArticuloClient();

			try
			{
				servicio.LanzarExcepcion();
			}
			catch (System.ServiceModel.FaultException<Modelos.Excepciones.PulperiaFault> pulperiaFault)
			{
				obtenido = pulperiaFault.Detail.Mensaje;
			}
			finally
			{
				servicio.Close();
			}

			Assert.AreEqual(esperado, obtenido);
		}
	}
}
