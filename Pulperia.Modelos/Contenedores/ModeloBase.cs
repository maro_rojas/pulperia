﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Pulperia.Modelos
{



	///<summary>
	///Modelo base del cual todos los modelos deben heredar
	///</summary>
	///<remarks></remarks>
	[Serializable()]
	[DataContract()]
	[KnownType(typeof(ModeloBase))]
	public class ModeloBase
	{


	}
}
