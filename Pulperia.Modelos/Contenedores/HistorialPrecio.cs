﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Modelos.Contenedores
{
    [DataContract]
    [Serializable]
    public partial class HistorialPrecio
    {
        [Key]
        [DataMember]
        public int IdHistorialPrecio { get; set; }
        [DataMember]
        public string CodArticulo { get; set; }
        [DataMember]
        public int Precio { get; set; }
        [DataMember]
        public int Mes { get; set; }

        public override string ToString()
        {
            return string.Format("CodArticulo: {0} - Mes: {1} - Precio {2}", CodArticulo, Mes, Precio);
        }
    }
}
