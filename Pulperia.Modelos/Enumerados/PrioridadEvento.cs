﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Modelos.Enumerados
{
	public enum PrioridadEvento : short
	{
		Ninguno = 0,
		Alta = 1,
		Media = 2,
		Baja = 3,
	}
}
