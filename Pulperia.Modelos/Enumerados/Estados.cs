﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Modelos.Enumerados
{
	[Serializable]
	[DataContract]
	public enum Estados : int
	{
		[EnumMember(Value = "Ninguno"), Description("Ninguno")]
		Ninguno = 0,
		[EnumMember(Value = "Inactivo"), Description("Inactivo")]
		Inactivo = 1,
		[EnumMember(Value = "Activo"), Description("Activo")]
		Activo = 2,
		[EnumMember(Value = "Eliminado"), Description("Eliminado")]
		Eliminado = 3,
	}
}
