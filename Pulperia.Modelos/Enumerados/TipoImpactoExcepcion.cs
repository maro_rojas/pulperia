﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Modelos.Enumerados
{
	public enum TipoImpactoExcepcion : short
	{
		Ninguno = 0,
		Alto = 1,
		Medio = 2,
		Bajo = 3,
	}
}
