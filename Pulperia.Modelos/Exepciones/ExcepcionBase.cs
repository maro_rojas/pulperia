﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Permissions;
using System.Runtime.Serialization;
using System.Collections;
using Pulperia.Modelos;
using Pulperia.Modelos.Enumerados;


[Serializable()]
public class ExcepcionBase : Exception
{
		#region "Declaraciones"

	private const string TagNumeroError = "mNumeroError";
	private const string TagDescripcion = "mDescripcion";
	private const string TagUbicacion = "mUbicacion";
	private const string TagFecha = "mFecha";

	private const string TagArgumentos = "mArgumentos";
	#endregion

	#region "Constructores"

	public int NumeroError { get; set; }
	public string Ubicacion { get; set; }
	public System.DateTime Fecha { get; set; }

	private List<object> mArgumentos;
	public virtual List<object> Argumentos
	{
		get { return mArgumentos; }
	}

	public ExcepcionBase()
		: base()
	{
		this.Fecha = DateTime.Now;
		mArgumentos = new List<object>();
	}

	public ExcepcionBase(string mensaje)
		: base(mensaje)
	{
		this.Fecha = DateTime.Now;
		mArgumentos = new List<object>();
	}

	/// <summary>
	/// Constructor con el mensaje de la excepción y la excepción origen
	/// </summary>
	/// <param name="mensaje">Mensaje de la excepción</param>
	/// <param name="excepcionInterna">Excepción origen</param>
	/// <remarks></remarks>
	public ExcepcionBase(string mensaje, System.Exception excepcionInterna)
		: base(mensaje, excepcionInterna)
	{
		this.Fecha = DateTime.Now;
		mArgumentos = new List<object>();
	}

	protected ExcepcionBase(SerializationInfo info, StreamingContext contexto)
		: base(info, contexto)
	{
		this.NumeroError = (int)info.GetValue(TagNumeroError, typeof(int));
		this.Ubicacion = (string)info.GetValue(TagUbicacion, typeof(string));
		this.Fecha = (DateTime)info.GetValue(TagFecha, typeof(System.DateTime));
		mArgumentos = (List<object>)info.GetValue(TagArgumentos, typeof(List<object>));
	}

	public ExcepcionBase(string mensaje, int numError, string ubicacion, params object[] argumentos)
		: base(mensaje)
	{
		this.NumeroError = numError;
		this.Ubicacion = ubicacion;
		this.Fecha = DateTime.Now;
		object argumento = null;
		mArgumentos = new List<object>();
		foreach (object argumento_loopVariable in argumentos)
		{
			argumento = argumento_loopVariable;
			mArgumentos.Add(argumento);
		}
	}

	/// <summary>
	/// Constructor con colección de información adicional y las acciones a realizar de tipo array
	/// </summary>
	/// <param name="mensaje">Es el mensaje de la excepción</param>
	/// <param name="excepcionInterna">Es la excepción a manipular como interna</param>
	/// <param name="datos">Es la información adicional de la excepción</param>
	/// <param name="acciones">Es la lista de acciones a realizar para revisar algún problema presentado</param>
	/// <remarks></remarks>
	public ExcepcionBase(string mensaje, System.Exception excepcionInterna, System.Collections.Generic.IDictionary<string, string> datos, System.Collections.Generic.IEnumerable<string> acciones)
		
	{

	}

	#endregion

	#region "Métodos Públicos"

	/// <summary>
	/// Método para la serialización de la excepción mediante ISerializable
	/// </summary>
	/// <param name="info"></param>
	/// <param name="context"></param>
	/// <remarks></remarks>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule")]
	[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
	public override void GetObjectData(SerializationInfo info, StreamingContext context)
	{
		base.GetObjectData(info, context);
		info.AddValue(TagNumeroError, this.NumeroError, typeof(int));
		info.AddValue(TagUbicacion, this.Ubicacion, typeof(string));
		info.AddValue(TagFecha, this.Fecha, typeof(System.DateTime));
		info.AddValue(TagArgumentos, mArgumentos, typeof(List<object>));
	}

	#endregion

}