﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pulperia.Modelos;

namespace Pruebas.Consola
{
	class Program
	{
		static void Main(string[] args)
		{
			var servicio = new Servicio.Articulo.ArticuloClient();

			try
			{
				var listaArticulos = servicio.ListarDesdeBD();

				foreach (Articulo a in listaArticulos)
				{
					var cadena = string.Format("Codido: {0} \nProducto: {1} \nPrecio: {2}\n--------------------", 
						a.CodArticulo, a.Nombre, a.Precio);
					Console.WriteLine(cadena);
				}
				Console.WriteLine("/n/n/n/n/n");
				Console.WriteLine("Escriba el codigo del producto");
				string cod = Console.ReadLine();
				var art = servicio.ObtenerPorCodArticulo(cod);
				Console.WriteLine("Articulo: " + art.Nombre + "Cod:" + art.CodArticulo);
				Console.ReadKey();
			}
			finally
			{
				servicio.Close();
			}
		}
	}
}
