﻿
using Pulperia.Pruebas.Comunes.Contenedores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Pruebas.Comunes.Ayudantes
{
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
	public abstract class AyudanteBase<T> where T : EscenarioBase
	{

		/// <summary>
		/// Carga los escenarios desde el archivo de insumo a una colección de objetos que los representa.
		/// </summary>
		/// <returns>Lista que contine los escenarios del archivo de insumo</returns>
		public abstract List<T> CargarEscenarios();

		/// <summary>
		/// Recibe la lista de los escenacios de la prueba
		/// Ejecuta procesos miscelaneos para preparar el ambiente previo a procesar los escenarios
		/// </summary>
		/// <param name="listaEscenarios"></param>
		public abstract void InicializarPrueba(List<T> listaEscenarios);

		/// <summary>
		/// Lleva a cabo la ejecución de los escenarios
		/// </summary>
		/// <param name="listaEscenarios"></param>
		public abstract void ProcesarEscenarios(IEnumerable<T> listaEscenarios);


	}
}