﻿using System;
using Pulperia.Pruebas.Comunes.Contenedores;
using System.Collections.Generic;

namespace Pulperia.Pruebas.Comunes.Verificadores
{
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
	public abstract class VerificadorBase<T> where T : EscenarioBase
	{

		protected string resultadoEscenario;
		protected string resultadoValidaciones;

		protected abstract string EncabezadoEscenario(T escenario);

		protected void AgregarErrorEscenario(string resultado)
		{
			if (!string.IsNullOrEmpty(resultado))
				resultadoEscenario += "- " + resultado + Environment.NewLine;
		}

		protected void AgregarResultadoGeneral(string resultadoActual)
		{
			if (!string.IsNullOrEmpty(resultadoActual))
				resultadoValidaciones += resultadoActual + Environment.NewLine;
		}

		protected void ReportarResultadoEscenario(string encabezado)
		{
			if (!string.IsNullOrEmpty(resultadoEscenario))
				resultadoEscenario = encabezado + Environment.NewLine + resultadoEscenario;
		}

		protected abstract void CompletarEscenario(T escenario);

		public abstract string VerificarEscenarios(List<T> escenarios);

		protected abstract void VerificarEscenarioExitoso(T escenario);

		protected abstract void VerificarEscenarioNoExitoso(T escenario);

	}
}

