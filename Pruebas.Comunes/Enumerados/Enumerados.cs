﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Pruebas.Comunes.Enumeradores
{
	public enum CodigosEstado : int
	{
		[EnumMember(Value = "NoExiste"), Description("NoExiste")]
		NoExiste = 0,
		[EnumMember(Value = "Inactivo"), Description("Inactivo")]
		Inactivo = 1,
		[EnumMember(Value = "Activo"), Description("Activo")]
		Activo = 2,
	}
}
