﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.Pruebas.Comunes.Contenedores
{
	public interface IEscenario
	{
		string Resultado { get; set; }

		string MensajeErrorRecibido { get; set; }
	}
}
