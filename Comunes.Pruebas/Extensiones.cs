﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Comunes.Pruebas
{
	public static class Extensiones
	{

		#region DataReader

		/// <summary>
		/// Retorna una lista de objetos que contienen la información del DataReader
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dr"></param>
		/// <param name="inicializarValoresPorDefecto">Indica si se deben inicializar los valores por defecto de las propiedades</param>
		/// <returns></returns>
		public static List<T> ToList<T>(this IDataReader dr, bool inicializarValoresPorDefecto = true) where T : class
		{
			var ret = new List<T>();
			var objType = typeof(T);
			var vProperties = objType.GetProperties();

			while (dr.Read())
			{
				var vColumns = new List<string>();

				for (int i = 0; i < dr.FieldCount; i++)
					vColumns.Add(dr.GetName(i).Trim());

				//creamos el objeto
				var obj = Activator.CreateInstance(objType);
				//cargamos los valores para cada fila
				foreach (PropertyInfo p in vProperties)
				{
					if (vColumns.Contains(p.Name))
					{
						var value = dr[p.Name];
						if (value != null && value != DBNull.Value)
							p.SetValue(obj, Convert.ChangeType(dr[p.Name], p.PropertyType), null);
						else if (inicializarValoresPorDefecto)
							p.SetValue(obj, GetDafaultValue(p.PropertyType), null);
					}
				}
				ret.Add((T)obj);
			}
			if (!dr.IsClosed)
				dr.Close();
			return ret;
		}

		static object GetDafaultValue(Type propertyType)
		{
			// Get the type code so we can switch
			System.TypeCode typeCode = System.Type.GetTypeCode(propertyType);

				switch (typeCode)
				{
					case TypeCode.Boolean:
						return false;
					case TypeCode.SByte:
					case TypeCode.Byte:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
					case TypeCode.UInt64:
					case TypeCode.Int16:
					case TypeCode.Int32 :
					case TypeCode.Int64:
					case TypeCode.Single:
					case TypeCode.Double:
					case TypeCode.Decimal:
						return 0;
					case TypeCode.Char:
					case TypeCode.String:
						return string.Empty;
					case TypeCode.DateTime:
						return DateTime.MinValue;
					case TypeCode.Object:
						if (propertyType == typeof(Guid) || propertyType == typeof(Guid?))
						{
							return Guid.Empty;
						}
						break;
				}
				return null;
		}
		#endregion
	}
}