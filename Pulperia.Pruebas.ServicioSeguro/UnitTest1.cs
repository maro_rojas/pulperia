﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Pulperia.Pruebas.ServicioSeguro
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var resultado = false;
            var servicio = new ServicioSeguro.ServicioSeguroClient();

            try
            {
                resultado = servicio.ServicioDisponible();
            }
            finally
            {
                servicio.Close();
            }

            Assert.IsTrue(resultado);
        }
    }
}
