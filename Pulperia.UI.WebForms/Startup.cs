﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pulperia.UI.WebForms.Startup))]
namespace Pulperia.UI.WebForms
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
