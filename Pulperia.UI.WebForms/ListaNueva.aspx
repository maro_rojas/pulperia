﻿
<%@ Page Title="Listar Nueva" Language="C#"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListarArticulos.aspx.cs" Inherits="Pulperia.UI.WebForms.ListarArticulos" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div>
		Listar Nueva
		<div class="col-md-offset-2 col-md-10">
			<input value="Buscar" onclick="buscar()" type="button" class="btn btn-default" />
		</div>
		<div id="result"></div>
		<script>
			function replacer(key, value) {
				for (var val in this) {
					if (typeof (this[val]) === 'string')
						this[val] = this[val].toUpperCase();
					else
						this[val] = this[val]
				}
				return value;
			}

			function buscar() {

				$.ajax({
					url: "http://localhost/Servicios/Pulperia.Servicios/Articulo.svc/Articulos",
					type: "GET",
					contentType: "application/json",
					dataType: "JSON"
				}).done(function (json)
				{

					$("#result").html(JSON.stringify(json, replacer));

				}).fail(function ()
				{
					$("#result").html("Ha ocurrido un problema");
				});
			}
		</script>
	</div>
    
	</asp:Content>