﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ArticuloRestful.aspx.cs" Inherits="Pulperia.UI.WebForms.ArticuloRestful" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		Código de artículo
        <input type="text" id="CodArticulo" class="form-control" />
		<br />
		<div class="col-md-offset-2 col-md-10">
			<input value="Buscar" onclick="buscar()" type="button" class="btn btn-default" />
		</div>
		<div id="result"></div>
		<script>
			function buscar() {
				var data = $("#CodArticulo").val();

				$.ajax({
					url: "http://localhost/Servicios/Pulperia.Servicios/Articulo.svc/ObtenerArticulos/" + $("#CodArticulo").val(),
					type: "POST",
					contentType: "application/json",
					data: JSON.stringify(data),
					dataType: "JSON"
				}).done(function (json)
				{
					$("#result").html(JSON.stringify(json));
				}).fail(function ()
				{
					$("#result").html("Ha ocurrido un problema");
				});
			}
		</script>
	</div>
</asp:Content>
