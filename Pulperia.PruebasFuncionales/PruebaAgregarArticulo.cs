﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pulperia.PruebasFuncionales.Ayudantes;
using Pulperia.PruebasFuncionales.Contenedores;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pulperia.PruebasFuncionales
{
    [TestClass]
	public class PruebaAgregarArticulo
    {
		[TestMethod]
		public void ServicioDisponible_Pruebas()
		{
			var resultado = false;
			var servicio = new Utilitario.Articulo.UtilitarioClient();
			try
			{
				resultado = servicio.ServicioDisponible();

			}
			finally
			{
				servicio.Close();
			}
			Assert.IsTrue(resultado);
		}

		[ClassInitialize()]
		public static void Inicializar(TestContext context)
		{

		}

		[TestInitialize()]
		public void InicializarPruebas()
		{

		}

		[TestMethod()]
		[TestCategory("Funcional")]
		[TestCategory("SinpeMovil")]
		[TestCategory("Logica Negocio")]
		[TestCategory("Suscripcion")]
        public void AgregarArticulo_Prueba()
        {
            var resultadoObtenido = "El resultado Obtenido debe ser igual al esperado";
			var ayudantePruebaAgregarArticulo = new AyudantePruebaAgregarArticulo();

            //Paso 1: Cargar escenarios
			IEnumerable<EscenarioArticulo> listaEscenarios = ayudantePruebaAgregarArticulo.CargarEscenarios();

			//Paso 2: Preparar ambiente local
			ayudantePruebaAgregarArticulo.InicializarPrueba(listaEscenarios);

            //Paso 3: Procesar los escenarios de la prueba
			ayudantePruebaAgregarArticulo.ProcesarEscenarios(listaEscenarios);

            //Paso 4: Generar informe de la ejecución de los escenarios
			resultadoObtenido = ayudantePruebaAgregarArticulo.VerificarEscenarios(listaEscenarios);

			//Paso 5: Verificar el resultado de los escenarios
            Assert.AreEqual("", resultadoObtenido);

        }

        [TestCleanup()]
        public void Finalizar()
        {

        }

    }
}
