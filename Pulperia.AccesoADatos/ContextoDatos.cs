using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Pulperia.Modelos;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Pulperia.AccesoDatos
{

	public partial class ContextoDatos : DbContext
	{
		public ContextoDatos()
			: base("name=Pulperia")
		{
			this.Configuration.LazyLoadingEnabled = false;
			this.Configuration.ProxyCreationEnabled = false;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}
	}
}
