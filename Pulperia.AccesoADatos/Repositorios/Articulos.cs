﻿using Pulperia.IAccesoDatos;
using Pulperia.Modelos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.AccesoDatos.Repositorios
{
	[UnityAutoRegistration()]
	public sealed class Articulos : ContextoDatos
	{
		public DbSet<Articulo> Articulo { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}


}
