﻿using Pulperia.Modelos.Contenedores;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulperia.AccesoDatos.Repositorios
{
    public class HistorialPrecios : ContextoDatos
    {
        public virtual DbSet<HistorialPrecio> HistorialPrecio { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
